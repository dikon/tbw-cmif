Chronologisches Briefverzeichnis

1. Rat der Stadt Leipzig an Thomasius, [Leipzig, Anfang 1679]
2. Thomasius an den Rat der Stadt Leipzig, Leipzig, 10. März 1679
3. Thomasius an den Schöppenstuhl zu Leipzig, Leipzig, 7. September 1679
4. Thomasius an den Schöppenstuhl zu Leipzig, Leipzig, 4. Juni 1681
5. Adam Christoph Jacobi an Thomasius, Dresden, 1. Dezember 1682
6. Thomasius an Adam Christoph Jacobi, [Leipzig, Ende Dezember 1682]
7. Adam Christoph Jacobi an Thomasius, Dresden, 2. Januar 1683
8. Thomasius an Adam Christoph Jacobi, [Leipzig, Februar 1683]
9. Adam Christoph Jacobi an Thomasius, Dresden, 20. Februar 1683
10. Jacob Born an Thomasius, Dresden, 21. Juli 1683
11. Thomasius an Jacob Born, [Leipzig, wahrsch. Mitte August 1683]
12. Thomasius an Adam Christoph Jacobi, [Leipzig, zwischen 24. und 27. August 1683]
13. Adam Christoph Jacobi an Thomasius, Dresden, 28. August 1683
14. Jacob Born an Thomasius, Dresden, 28. August 1683
15. Thomasius an Jacob Born, [Leipzig, Ende August/Anfang September 1683]
16. Thomasius an Adam Christoph Jacobi, [Leipzig, Ende August/Anfang September 1683]
17. Adam Christoph Jacobi an Thomasius, [Dresden, wahrsch. Anfang/Mitte September 1683]
18. Jacob Born an Thomasius, Dresden, 4. September 1683
19. Jacob Born an Thomasius, [Dresden], 15. September 1683
20. Jacob Born an Thomasius, Dresden, 26. Januar [1684]
21. Thomasius an Kurfürst Johann Georg III. von Sachsen, [Leipzig, Ende Januar/ Februar 1684]
22. Thomasius an Adam Christoph Jacobi, [Leipzig, Juli/Anfang August 1684]
23. Adam Christoph Jacobi an Thomasius, Dresden, 12. August 1684
24. Adam Christoph Jacobi an Thomasius, Dresden, 6. März 1685
25. Thomasius an Samuel Pufendorf, [Leipzig], 11. April [1685]
26. Samuel Pufendorf an Thomasius, [Stockholm, wahrsch. Mai 1685]
27. Thomasius an Jacob Born, [Leipzig, November 1685]
28. Jacob Born an Thomasius, Dresden, 2. Dezember [1685]
29. Thomasius an Samuel Pufendorf, [Leipzig], 22. April 1686
30. Samuel Pufendorf an Thomasius, Stockholm, 9. Juni 1686
31. Philipp Jacob Spener an Thomasius, [Dresden, 5. November 1686]
32. Thomasius an Philipp Jacob Spener, Leipzig, 10. November 1686
33. Philipp Jacob Spener an Thomasius, [Dresden, vor dem 7. Dezember 1686]
34. Gottfried Thomasius an Christian Thomasius, [Utrecht, ca. Anfang 1687]
35. Thomasius an Samuel Pufendorf, [Leipzig], 1. Februar 1687
36. Johann Georg Graevius an Thomasius, [Utrecht, evtl. Anfang/Mitte März 1687]
37. Thomasius an Johann Georg Graevius, Leipzig, 23. März 1687
38. Samuel Pufendorf an Thomasius, Stockholm, 9. April 1687
39. Thomasius an Samuel Pufendorf, [Leipzig], 7. Mai 1687
40. Samuel Pufendorf an Thomasius, Stockholm, 31. August 1687
41. Thomasius an Samuel Pufendorf, [Leipzig], 5. Oktober 1687
42. Ein „auswärtiger Freund“ bzw. „Bekandter“ an Thomasius, [vermutl. November] 1687
43. Thomasius an den Rat der Stadt Leipzig, Leipzig, 23. Januar 1688
44. Thomasius an das Oberkonsistorium Dresden, [Leipzig, ca. 25. Januar 1688]
45. Samuel von Pufendorf an Thomasius, Berlin, 11. Februar 1688
46. Thomasius an Samuel von Pufendorf, [Leipzig], 18. Februar 1688
47. Samuel von Pufendorf an Thomasius, Berlin, 25. Februar 1688
48. Thomasius an Samuel von Pufendorf, [Leipzig], 4. März 1688
49. Samuel von Pufendorf an Thomasius, Berlin, 14. März 1688
50. Thomasius an Samuel von Pufendorf, [Leipzig], 17. März 1688
51. Samuel von Pufendorf an Thomasius, Berlin, 24. März 1688
52. Thomasius an Samuel von Pufendorf, [Leipzig, ca. 27. März] 1688
53. Samuel von Pufendorf an Thomasius, Berlin, 31. März 1688
54. Thomasius an Friedrich Adolph von Haugwitz, [Leipzig], 1. April 1688
55. Friedrich Adolph von Haugwitz an Thomasius, [Dresden], 4. April 1688
56. Thomasius an Friedrich Adolph von Haugwitz, [Leipzig], 10. April 1688
57. Thomasius an Samuel von Pufendorf, [Leipzig], 12. Mai 1688
58. Samuel von Pufendorf an Thomasius, Berlin, 15. Mai 1688
59. Thomasius an Samuel von Pufendorf, [Leipzig], 8. Juni 1688
60. Samuel von Pufendorf an Thomasius, Berlin, 19. Juni 1688
61. Thomasius an Samuel von Pufendorf, [Leipzig], 23. Juni 1688
62. Thomasius an „Herrn W.“, [Leipzig, vermutl. Ende Juni/Anfang Juli 1688]
63. Thomasius an Samuel von Pufendorf, [Leipzig], 4. Juli 1688
64. Johann Jacob Stübel an Thomasius, Annaberg, 7. Juli 1688
65. Samuel von Pufendorf an Thomasius, Berlin, 17. Juli 1688
66. Thomasius an Friedrich Adolph von Haugwitz, [Leipzig], 22. Juli 1688
67. Thomasius an Samuel von Pufendorf, [Leipzig], 22. Juli 1688
68. Samuel von Pufendorf an Thomasius, Berlin, 25. Juli 1688
69. Thomasius an Johann Jacob Stübel, [Leipzig, Ende Juli] 1688
70. Samuel von Pufendorf an Thomasius, Berlin, 11. August 1688
71. Johann Jacob Stübel an Thomasius, Annaberg, 20. August 1688
72. Thomasius an Samuel von Pufendorf, [Leipzig], 22. August 1688
73. Johann Jacob Stübel an Thomasius, [Annaberg], 26. August 1688
74. Samuel von Pufendorf an Thomasius, Berlin, 28. August 1688
75. Thomasius an Samuel von Pufendorf, [Leipzig], 12. September 1688
76. Samuel von Pufendorf an Thomasius, Berlin, 18. September 1688
77. Johann Georg Kulpis an Thomasius, [Stuttgart, Anfang Oktober] 1688
78. Thomasius an Samuel von Pufendorf, [Leipzig], 7. Oktober 1688
79. Thomasius an Samuel von Pufendorf, [Leipzig], 10. Oktober 1688
80. Samuel von Pufendorf an Thomasius, Berlin, 16. Oktober 1688
81. Tobias Pfanner an Thomasius, Weimar, 15. November 1688
82. Thomasius an Samuel von Pufendorf, [Leipzig], 24. November 1688
83. Thomasius an Tobias Pfanner, Leipzig, 25. November 1688
84. Enoch Zobel an Thomasius, [Annaberg, ca. 26. November] 1688
85. Thomasius an Enoch Zobel, [Leipzig, ca. 28. November] 1688
86. Thomasius an Johann Jacob Stübel, [Leipzig, ca. 28. November] 1688
87. Tobias Pfanner an Thomasius, Weimar, 29. November 1688
88. Enoch Zobel an Thomasius, [Annaberg, ca. 29./30. November] 1688
89. Samuel von Pufendorf an Thomasius, Berlin, 1. Dezember 1688
90. Johann Jacob Stübel an Thomasius, Annaberg, 3. Dezember 1688
91. [Johann Christian] Lange an Thomasius, [Frankfurt/Main], 7. Dezember 1688
92. Thomasius an Samuel von Pufendorf, [Leipzig], 8. Dezember 1688
93. Samuel von Pufendorf an Thomasius, Berlin, 15. Dezember 1688
94. Thomasius an Samuel von Pufendorf, [Leipzig], 22. Dezember 1688
95. Thomasius an Tobias Pfanner, Leipzig, 23. Dezember 1688
96. [Johann Christian] Lange an Thomasius, Frankfurt [am Main], 28. Dezember 1688
97. Samuel von Pufendorf an Thomasius, Berlin, 30. Dezember 1688
98. „M. A.“ an Thomasius, [Ende 1689/Anfang 1690]
99. Thomasius an Tobias Pfanner, Leipzig, 13. Januar 1689
100. Tobias Pfanner an Thomasius, Weimar, 14. Januar 1689
101. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 14. Januar 1689
102. Johann Joachim Rothe an Thomasius, [Leipzig, ca. 14. Januar 1689]
103. Thomasius an Kurfürst Johann Georg III. von Sachsen bzw. das Geheime Ratskollegium, [Leipzig], 15. Januar 1689
104. Thomasius an Samuel von Pufendorf, [Leipzig], 23. Januar 1689
105. Thomasius an Kurfürst Johann Georg III. von Sachsen, Leipzig, 24. Januar 1689
106. Thomasius an das Oberkonsistorium Dresden, [Leipzig], 24. Januar 1689
107. Friedrich Adolph von Haugwitz an Thomasius, [Dresden, Anfang Februar 1689]
108. Thomasius an Hans Ernst von Knoch, [Leipzig], 10. Februar 1689
109. Thomasius an Friedrich Adolph von Haugwitz, [Leipzig], 10. Februar 1689
110. Johann Jacob Stübel an Thomasius, Annaberg, 25. Februar 1689
111. Thomasius an Johann Joachim Rothe, [Leipzig, 25. Februar 1689]
112. Tobias Pfanner an Thomasius, Weimar, 14. März 1689
113. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig, vermutl. Mitte März 1689]
114. Thomasius an das Oberkonsistorium Dresden, [Leipzig], 28. März 1689
115. Thomasius an Samuel von Pufendorf, [Leipzig], 3. April 1689
116. Thomasius an das Concilium perpetuum der Universität Leipzig, [Leipzig], 4. April 1689
117. Thomasius an Hans Haubold von Einsiedel, [Leipzig], 7. April 1689
118. Samuel von Pufendorf an Thomasius, Berlin, 10. April 1689
119. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 15. April 1689
120. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 19. April 1689
121. Thomasius an das Concilium perpetuum der Universität Leipzig, [Leipzig, 19. April 1689]
122. [Johannes] Müller an Thomasius, Hamburg, [kurz vor dem 21. April] 1689
123. Ungenannter Bedienter des Oberhofmarschalls von Haugwitz an Thomasius, [Dresden], 23. April 1689
124. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 31. Mai 1689
125. Thomasius an Samuel von Pufendorf, [Leipzig], 8. Juni 1689
126. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 10. Juni 1689
127. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 14. Juni 1689
128. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 21. Juni 1689
129. Thomasius an das kursächsische Geheime Ratskollegium, [Leipzig], 27. Juni 1689
130. Thomasius an Hans Ernst von Knoch, [Leipzig], 27. Juni 1689
131. Thomasius an das Oberkonsistorium Dresden, [Leipzig], 27. Juni 1689 (I)
132. Thomasius an das Oberkonsistorium Dresden, [Leipzig], 27. Juni 1689 (II)
133. Thomasius an Georg Lehmann, [Leipzig], 3. Juli 1689
134. Thomasius an das Geistliche Ministerium zu Leipzig, [Leipzig], 3. Juli 1689
135. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 13. Juli 1689 (I)
136. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 13. Juli 1689 (II)
137. Thomasius an das Oberkonsistorium Dresden, [Leipzig], 20. Juli 1689
138. Thomasius an Georg Lehmann, [Leipzig], 6. August 1689
139. Samuel von Pufendorf an Thomasius, Berlin, 7. August 1689
140. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 10. August 1689
141. Georg Lehmann an Thomasius, [Leipzig, 19. oder 20. August 1689]
142. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 20. August 1689
143. Thomasius an das Concilium perpetuum der Universität Leipzig, [Leipzig, 21. August 1689]
144. Thomasius an Samuel von Pufendorf, [Leipzig], 21. August 1689
145. Thomasius an das Concilium perpetuum der Universität Leipzig, [Leipzig, 23. August 1689]
146. Thomasius an das Concilium perpetuum der Universität Leipzig, [Leipzig], 24. August 1689
147. Samuel von Pufendorf an Thomasius, Berlin, 28. August 1689
148. Thomasius an das Concilium perpetuum der Universität Leipzig, [Leipzig], 2. September 1689
149. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 23. September 1689
150. Thomasius an Herzog Moritz Wilhelm von Sachsen-Zeitz, [Leipzig], Anfang Januar 1690
151. Herzog Moritz Wilhelm von Sachsen-Zeitz an Thomasius, [Zeitz], Anfang Januar 1690
152. Thomasius an Samuel von Pufendorf, [Leipzig], 26. Januar 1690
153. Ungenannter Freund an Thomasius, [Berlin, wahrsch. Anfang/Mitte Februar] 1690
154. Samuel von Pufendorf an Thomasius, Berlin, 4. Februar 1690
155. Concilium perpetuum der Universität Leipzig an Thomasius, [Leipzig], 20. Februar 1690
156. Thomasius an Johann Christoph Becmann, Leipzig, 15. März 1690
157. Thomasius an das Concilium perpetuum der Universität Leipzig, [Leipzig], 17./18. März 1690
158. Thomasius an das Oberkonsistorium Dresden, [Berlin, nach dem 21. März 1690]
159. Thomasius an Kurfürst Friedrich III. von Brandenburg, Berlin, 25. März 1690
160. Thomasius an Johann Alexander Christ, Berlin, [vermutl. Ende März/Anfang April] 1690
161. Kurfürst Friedrich III. von Brandenburg an Thomasius, Königsberg, 4./14. April 1690
162. Johann Alexander Christ an Thomasius, [Leipzig, vermutl. Anfang April] 1690
163. Thomasius an einen unbekannten Empfänger [Johann Christoph Becmann?], Berlin, 11. April 1690
164. Thomasius an Kurfürst Friedrich III. von Brandenburg, Berlin, 15. April 1690
165. Thomasius an Samuel von Pufendorf, [Halle], 29. April 1690
166. Samuel von Pufendorf an Thomasius, Berlin, 18. Mai 1690
167. Anonyme(r) Absender an Thomasius, [Dänemark, wahrsch. zweite Jahreshälfte 1690]
168. Unbekannter Absender an Thomasius, [wahrsch. zweite Jahreshälfte 1690]
169. Thomasius an den Rat der Stadt Halle, Halle, 7. August 1690
170. Rat der Stadt Halle an Thomasius, Halle, 9. August 1690
171. Thomasius an August Hermann Francke, [Halle, wahrsch. Mitte/Ende August 1690]
172. Thomasius an die Magdeburgische Regierung zu Halle, Halle, 18. August 1690
173. Thomasius an Kurfürst Friedrich III. von Brandenburg, Halle, 26. August 1690 (I)
174. Thomasius an Kurfürst Friedrich III. von Brandenburg, Halle, 26. August 1690 (II)
175. Thomasius an Samuel von Pufendorf, [Halle], 26. August 1690
176. Thomasius an Johann Georg II., Fürst von Anhalt-Dessau, Halle, 30. August 1690
177. Thomasius an Samuel von Pufendorf, [Halle], 30. August 1690
178. August Hermann Francke an Thomasius, [Erfurt, wahrsch. August/Anfang September 1690]
179. Magdeburgische Regierung zu Halle an Thomasius, Halle, 2. September 1690
180. Samuel von Pufendorf an Thomasius, Berlin, 2. September 1690
181. Kurfürst Friedrich III. von Brandenburg an Thomasius, Cölln, 3. September 1690
182. August Hermann Francke an Thomasius, Erfurt, 8. September 1690
183. Thomasius an Kurfürst Johann Georg III. von Sachsen, Halle, 15. September 1690
184. [Kursächsisches Geheimes Ratskollegium] an Thomasius, [Dresden, Mitte/Ende September] 1690
185. Kurfürst Friedrich III. von Brandenburg an Thomasius, Cölln, 30. September 1690
186. Thomasius an Kurfürst Friedrich III. von Brandenburg, Halle, 6. Oktober 1690
187. Thomasius an Samuel von Pufendorf, [Halle], 28. Oktober 1690
188. Samuel von Pufendorf an Thomasius, Berlin, 1. November 1690
189. Johann Joachim Wolf an Thomasius, Magdeburg, 13. November 1690
190. Thomasius an die Magdeburgischen Landstände, Halle, 22. November 1690
191. Magdeburgische Landstände an Thomasius, Magdeburg, 28. November 1690
192. Moritz Georg Weidmann an Thomasius, Leipzig, 11. Dezember 1690
193. Moritz Georg Weidmann an Thomasius, Leipzig, 1. Januar 1691
194. Thomasius an Moritz Georg Weidmann, [Halle, 2. oder 3. Januar] 1691
195. Moritz Georg Weidmann an Thomasius, Leipzig, 4. Januar 1691
196. Thomasius an Moritz Georg Weidmann, [Halle], 5. Januar 1691
197. Moritz Georg Weidmann an Thomasius, Leipzig, 7. Januar 1691
198. Moritz Georg Weidmann an Thomasius, [Leipzig, nach dem 7. Januar 1691]
199. Christianus Philothomas [Vincent Placcius] an Thomasius, [Hamburg], 1. Februar 1691
200. Thomasius an Moritz Georg Weidmann, [Halle], 7. Februar 1691
201. Moritz Georg Weidmann an Thomasius, Leipzig, 11. Februar 1691
202. Thomasius an Moritz Georg Weidmann, [Halle, Ende Februar/Anfang März] 1691
203. Moritz Georg Weidmann an Thomasius, Leipzig, 6. März 1691
204. Thomasius an Moritz Georg Weidmann, [Halle], 10. März 1691
205. Thomasius an Gottfried von Jena und Carl Ernst Krause, Halle, 11. März 1691
206. Moritz Georg Weidmann an Thomasius, Leipzig, 14. März 1691
207. Thomasius an Johann Georg II., Fürst von Anhalt-Dessau, Halle, 17. März 1691
208. Thomasius an Samuel von Pufendorf, [Halle], 17. März 1691
209. Thomasius an Kurfürst Friedrich III. von Brandenburg, [Leipzig], 24. März 1691
210. Samuel von Pufendorf an Thomasius, Berlin, 24. März 1691
211. Kurfürst Friedrich III. von Brandenburg/Hof des Kurfürsten an Thomasius, [Cölln, Mitte bis Ende April] 1691
212. Hof des Kurfürsten Friedrich III. von Brandenburg an Thomasius, [Berlin/Cölln, nach dem 18. April] 1691
213. Thomasius an Samuel von Pufendorf, [Halle], 25. April 1691
214. Thomasius an Moritz Georg Weidmann, [Halle], 28. April 1691
215. Moritz Georg Weidmann an Thomasius, Leipzig, 30. April 1691
216. Samuel von Pufendorf an Thomasius, Berlin, 2. Mai 1691
217. Moritz Georg Weidmann an Thomasius, Leipzig, [Anfang Mai] 1691
218. Thomasius an Kurfürst Johann Georg III. von Sachsen, [Halle], Mai 1691
219. Hof des Kurfürsten Friedrich III. von Brandenburg an Thomasius, [Berlin/Cölln, vermutl. Mai] 1691
220. Kurfürst Friedrich III. von Brandenburg an Thomasius, Cölln, 6. Juni 1691
221. Thomasius an Samuel von Pufendorf, [Halle, Mitte Juni 1691]
222. Thomasius an Kurfürst Friedrich III. von Brandenburg, Halle, 16. Juni 1691
223. Hof des Kurfürsten Friedrich III. von Brandenburg an Thomasius, [Berlin/Cölln, vermutl. Mitte Juni 1691]
224. Samuel von Pufendorf an Thomasius, Berlin, 27. Juni 1691
225. Thomasius an den Hof des Kurfürsten Friedrich III. von Brandenburg, [Halle, vermutl. Ende Juni 1691]
226. Thomasius an Samuel von Pufendorf, [Halle], 15. September 1691
227. Thomasius an Samuel von Pufendorf, [Halle], 17. Oktober 1691
228. Samuel von Pufendorf an Thomasius, Berlin, 21. Oktober 1691
229. Thomasius an Samuel von Pufendorf, [Halle], 24. Oktober 1691
230. Samuel von Pufendorf an Thomasius, Berlin, 31. Oktober 1691
231. Ein „vertrauter Freund“ an Thomasius, [Leipzig, vermutl. Herbst/Ende] 1691
232. Thomasius an Samuel von Pufendorf, [Halle], 3. November 1691
233. Thomasius an Eberhard Christoph Balthasar von Danckelmann, [Halle], 4. November 1691
234. Eberhard Christoph Balthasar von Danckelmann an Thomasius, [Berlin/Cölln, November/Anfang Dezember] 1691
235. Samuel von Pufendorf an Thomasius, Berlin, 20. Dezember 1691
236. Thomasius an Samuel von Pufendorf, [Halle], 23. Februar 1692
237. Thomasius an Paul von Fuchs, [Halle, 23.] Februar 1692
238. Thomasius an Kurfürst Friedrich III. von Brandenburg, [Halle, 23.] Februar 1692
239. Samuel von Pufendorf an Thomasius, Berlin, 27. Februar 1692
240. Thomasius, August Hermann Francke und Joachim Justus Breithaupt an Kurfürst Friedrich III. von Brandenburg, Halle, 5. März 1692
241. Jacques Valentin an Thomasius, Magdeburg, 9. [März] 1692
242. Kurfürst Friedrich III. von Brandenburg an Thomasius, Joachim Justus Breithaupt und August Hermann Francke, Cölln, 9. März 1692
243. Christian Friedrich Kraut an Thomasius, [Berlin, ca. 29. März 1692]
244. Thomasius an [Christian Friedrich Kraut], [Halle, ca. 2. April 1692]
245. Thomasius an Samuel von Pufendorf, [Halle], 5. April 1692
246. Samuel von Pufendorf an Thomasius, Berlin, 9. April 1692
247. Thomasius, Joachim Justus Breithaupt und August Hermann Francke an den Rat der Stadt Halle, Halle, 28. April 1692
248. Thomasius an Daniel Ludolph von Danckelmann, Halle, 30. April 1692
249. [Vincent Placcius] an Thomasius, [Hamburg, Ende April/Anfang Mai 1692]
250. Kurfürst Friedrich III. von Brandenburg an [Thomasius, Joachim Justus Breithaupt, August Hermann Francke], Cölln, 6. Mai 1692
251. Thomasius, Joachim Justus Breithaupt und August Hermann Francke an Kurfürst Friedrich III. von Brandenburg, Halle, 21. Mai 1692
252. Thomasius an [Kurfürst Friedrich III. von Brandenburg], Halle, 21. Mai 1692
253. Johann Christoph Becmann an Thomasius, [Frankfurt/O., wahrsch. Anfang Juni 1692]
254. Thomasius an Johann Christoph Becmann, Halle, 18. Juni 1692
255. Kurfürst Friedrich III. von Brandenburg an [Christian Thomasius, Joachim Justus Breithaupt und August Hermann Francke], Cölln, 20. Juni 1692
256. Thomasius an den Rat der Stadt Halle, Halle, [wahrsch. 6. Juli] 1692
257. Benjamin Fritsche an Thomasius, [Halle, 8. Juli 1692]
258. Thomasius an den Rat der Stadt Halle, Halle, 8. Juli 1692
259. Thomasius an August Hermann Francke, [Halle, kurz vor dem 3. September 1692]
260. Ein „guter Freund“ (I) an Thomasius, Berlin, Oktober 1692
261. Thomasius an Samuel von Pufendorf, [Halle], 8. November [1692]
262. Samuel von Pufendorf an Thomasius, Berlin, 26. November [1692]
263. Thomasius an Johann Burchard May, [Halle, Ende 1692]
264. Johann Christoph Becmann an Thomasius, Frankfurt [an der Oder], 6. Dezember 1692
265. Thomasius an Samuel von Pufendorf, [Halle], 17. Dezember 1692
266. Thomasius an Samuel von Pufendorf, [Halle], 20. Dezember 1692
267. [Johann Baptist Crophius] an Thomasius, Berlin, 30. Dezember 1692
268. Thomasius an August Hermann Francke, Halle, 31. Dezember 1692
