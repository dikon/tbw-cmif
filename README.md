# Briefwechsel von Christian Thomasius im CMI-Format

Gegenwärtig wird am [Interdisziplinären Zentrum für die Erforschung der Europäischen Aufklärung](https://www.izea.uni-halle.de/forschung/d-erschliessungsprojekte-und-editionen/briefe-von-und-an-christian-thomasius.html) und mit Förderung durch die [Deutsche Forschungsgemeinschaft](https://gepris.dfg.de/gepris/projekt/163372513) eine historisch-kritische Edition der Korrespondenz von Christian Thomasius in vier Bänden (1679–1728) erarbeitet. Der erste Band ist bereits erschienen:

> _Christian Thomasius Briefwechsel_. Band 1: _1679–1692_. Hg. v. Frank Grunert, Matthias Hambrock und Martin Kühnel unter Mitarbeit v. Andrea Thiele. Berlin; Boston: de Gruyter, 2017. DOI: [10.1515/9783110471328](https://doi.org/10.1515/9783110471328).

Darin führt ein „Chronologisches Briefverzeichnis“ (pp. XXXVII–XLIV) Absender, Empfänger, Ort und Datum der Briefe auf. Diese Metadaten der Korrespondenz wurden in das TEI-basierte [CMI-Format](https://correspsearch.net/en/participate.html) überführt. Die resultierende XML-Datei, die in [correspSearch](https://correspsearch.net/en/search.html?c=https://gitlab.informatik.uni-halle.de/dikon/tbw-cmif/-/raw/master/data/briefverzeichnis.xml) integriert wurde, ist [hier](./data/briefverzeichnis.xml) zu finden.
