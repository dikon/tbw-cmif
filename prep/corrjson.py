#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
script to extract correspondence data
from plain text file found at:

  ../edits/briefverzeichnis.txt

Creator:          D. Herre
GitLab:     dikon/tbw-cmif

Created:        2020-04-29
Last Modified:  2020-06-04
"""

import re
import json

# path to plain text file
PLAIN = "edits/briefverzeichnis.txt"
JSON = "data/briefverzeichnis.json"

# regular expressions
reCorrNum = re.compile("^\\d+\\.\\s")
reCorrRel = re.compile("\\san\\s")
reCorrUnits = re.compile(",\\s")
reCorrDate = re.compile("\\d+\\.\\s\\S+\\s\\d+")
reCorrAdd = re.compile("\\[.*?\\]")


def main():
    """"main routine to extract correspondence metadata from plain text"""
    lines = []
    # read lines from plain text file into list
    with open(PLAIN, 'r', encoding="utf-8") as f:
        lines = [l.strip() for l in f.readlines()][2:]  # exclude first two lines
    # collect correspondence metadata
    corrindex = {}
    # loop over lines
    for corr in lines:
        # extract number
        idx = reCorrNum.search(corr)
        idx = idx.group().strip().replace(".", "")
        # extract author
        left = reCorrNum.sub("", corr)
        left = reCorrRel.split(left)
        author = left[0]
        # extract addressee
        units = reCorrUnits.findall(left[1])
        if len(units) < 2:   # single addressee
            pivot = reCorrUnits.search(left[1])
            pivot = pivot.start()
        else:   # more than one addressee
            pivot = list(reCorrUnits.finditer(left[1]))[-2]
            pivot = pivot.start()
        addressee = left[1][:pivot]
        # extract place and date
        left = reCorrUnits.sub("", left[1][pivot:], 1)
        corrindex[idx] = {"author": author,
                          "addressee": addressee,
                          "place-date": left}

    # save results to json file
    with open(JSON, 'w', encoding="utf-8") as f:
        s = json.dumps(corrindex, ensure_ascii=False, indent=2)
        f.write(s)


if __name__ == '__main__':
    main()
