#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
script to translate correspondence metadata
to TEI-based CMI format from JSON data found at:

  ../data/briefverzeichnis.json

Creator:          D. Herre
GitLab:     dikon/tbw-cmif

Created:        2020-06-04
Last Modified:  2020-11-25
"""

import re
import json
from dateutil import parser
from datetime import datetime
from cmif import build, local

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ directory path of folder for output json files ~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

OUT = "data"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ read json files with correspondence metadata and ~~ #
# ~~ authority data for persons, organs and places ~~~~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

metadata = []
with open("data/briefverzeichnis.json", 'r', encoding='utf-8') as f:
    metadata = json.load(f)

persons = {}
with open("auth/persons.json", 'r', encoding='utf-8') as f:
    persons = json.load(f)

organs = {}
with open("auth/organs.json", 'r', encoding='utf-8') as f:
    organs = json.load(f)

places = {}
with open("auth/places.json", 'r', encoding='utf-8') as f:
    places = json.load(f)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ regular expressions for editorial additions ~~ #
# ~~ and multiple correspondents ~~~~~~~~~~~~~~~~~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

reCorrAdd = re.compile('\\[.*?\\]')
reCorrAdd2 = re.compile('\\[|\\]')
reCorrMult = re.compile('(,\\s|\\sund\\s)')

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ custom date parser for parsing german months ~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


class MonthParser(parser.parserinfo):
    MONTHS = [
        ('Januar'),
        ('Februar'),
        ('März'),
        ('April'),
        ('Mai',),
        ('Juni'),
        ('Juli'),
        ('August'),
        ('September'),
        ('Oktober'),
        ('November'),
        ('Dezember'),
    ]

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ functions to process data of persons, places and dates ~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


def identify(name):
    """
    identify entity/entities by name
    return element(s) <persName> or <orgName>
    """
    if name in persons:
        identifier = persons[name]              # GND identifier is present
        if name == "den Hof des Kurfürsten Friedrich III. von Brandenburg":
            name = "Hof des Kurfürsten Friedrich III. von Brandenburg"
        if identifier is not None:
            if type(identifier) == str:             # single author
                if name == "Thomasius":
                    name = "Christian Thomasius"
                return [build.tei_pers_name(name, "http://d-nb.info/gnd/" + identifier)]
            else:                                   # multiple authors
                res = reCorrMult.split(name)
                res = [r for r in res if not reCorrMult.search(r)]
                if not res[0].startswith("["):  # check for editorial additions
                    return [build.tei_pers_name(res[j] if res[j] != "Thomasius"
                                                else "Christian Thomasius",
                                                "http://d-nb.info/gnd/" + identifier[j])
                            if identifier[j] is not None
                            else build.tei_pers_name(res[j])
                            for j in range(len(res))]
                else:     # additions are present: multiple authors in brackets..
                    return [build.tei_pers_name(      # enclose every single author
                        "[" + reCorrAdd2.sub("", res[j]) + "]"  # in square brackets!
                        if reCorrAdd2.sub("", res[j]) != "Thomasius"
                        else "[Christian Thomasius]",
                        "http://d-nb.info/gnd/" + identifier[j])
                        if identifier[j] is not None
                        else build.tei_pers_name(
                        "[" + reCorrAdd2.sub("", res[j]) + "]")
                        for j in range(len(res))]
        return [build.tei_pers_name(name)]      # no GND identifier is present
    elif name in organs:
        if name.startswith("die"):
            if name == "die Magdeburgischen Landstände":
                return [build.tei_org_name("Magdeburgische Landstände", "http://d-nb.info/gnd/" + organs[name])] \
                    if organs[name] is not None else [build.tei_org_name("Magdeburgische Landstände")]
            else:
                return [build.tei_org_name(re.sub("^die\\s", "", name), "http://d-nb.info/gnd/" + organs[name])] \
                    if organs[name] is not None else [build.tei_org_name(re.sub("^die\\s", "", name))]
        elif name.startswith("das"):
            if name == "das kursächsische Geheime Ratskollegium":
                return [build.tei_org_name("Kursächsisches Geheimes Ratskollegium", "http://d-nb.info/gnd/" + organs[name])] \
                    if organs[name] is not None else [build.tei_org_name("Kursächsisches Geheimes Ratskollegium")]
            elif name == "das Geistliche Ministerium zu Leipzig":
                return [build.tei_org_name("Geistliches Ministerium zu Leipzig", "http://d-nb.info/gnd/" + organs[name])] \
                    if organs[name] is not None else [build.tei_org_name("Geistliches Ministerium zu Leipzig")]
            else:
                return [build.tei_org_name(re.sub("^das\\s", "", name), "http://d-nb.info/gnd/" + organs[name])] \
                    if organs[name] is not None else [build.tei_org_name(re.sub("^das\\s", "", name))]
        elif name.startswith("den"):
            return [build.tei_org_name(re.sub("^den\\s", "", name), "http://d-nb.info/gnd/" + organs[name])] \
                if organs[name] is not None else [build.tei_org_name(re.sub("^den\\s", "", name))]
        else:
            return [build.tei_org_name(name, "http://d-nb.info/gnd/" + organs[name])] \
                if organs[name] is not None else [build.tei_org_name(name)]
    else:
        print("could not identify entitiy with name:")
        print(name)
        print("")
        return None


def place(line):
    """
    extract place from given line
    return element <placeName>
    """
    place = line.split(",")[0].strip()
    if place in places:
        if not reCorrAdd2.search(place):
            return build.tei_place_name(
                place,
                "http://www.geonames.org/" + places[place])
        else:
            if re.match("^\\[.*\\]$", place):
                return build.tei_place_name(
                    place,
                    "http://www.geonames.org/" + places[place])
            else:
                if re.match("^\\[", place) and not place.endswith("]"):
                    return build.tei_place_name(
                        "[" + reCorrAdd2.sub("", place).strip() + "]",  # del " ", "[" and "]"
                        "http://www.geonames.org/" + places[place])
                else:
                    return build.tei_place_name(
                        place,
                        "http://www.geonames.org/" + places[place])
    else:
        print("could not find place in given line:")
        print(place)
        print("")
        return None


def date(line):
    """
    extract date from given line
    return element <date>
    """
    line = line.replace(" (I)", "").replace(" (II)", "")
    line_split = line.split(",")
    if len(line_split) > 1:
        match = reCorrAdd2.sub("", line_split[1].strip())   # del " ", "[" and "]"
    elif line_split[0] in ["[Ende 1689/Anfang 1690]",
                           "[vermutl. November] 1687",
                           "[wahrsch. zweite Jahreshälfte 1690]"]:
        match = reCorrAdd2.sub("", line_split[0].strip())   # del " ", "[" and "]"
    # JAHRESABSCHNITT
    if match == "wahrsch. zweite Jahreshälfte 1690":
        return build.tei_date(attrib_not_before="1690-07",
                              attrib_not_after="1690-12")
    elif match == "vermutl. Herbst/Ende 1691":
        return build.tei_date(attrib_not_before="1691-07",
                              attrib_not_after="1691-12")
    # ZEITSPANNE: ANFANG / ENDE JAHR
    elif match == "Anfang 1679":
        return build.tei_date(attrib_not_before="1679-01",
                              attrib_not_after="1679-01")
    elif match == "Ende 1692":
        return build.tei_date(attrib_not_before="1692-12",
                              attrib_not_after="1692-12")
    elif match == "Ende 1689/Anfang 1690":
        return build.tei_date(attrib_not_before="1689-12",
                              attrib_not_after="1690-01")
    # ZEITSPANNE: MONAT
    elif match == "Februar 1683":
        return build.tei_date(attrib_when="1683-02")
    elif match == "November 1685":
        return build.tei_date(attrib_when="1685-11")
    elif match == "wahrsch. Mai 1685":
        return build.tei_date(attrib_when="1685-05")
    elif match == "vermutl. Mai 1691":
        return build.tei_date(attrib_when="1691-05")
    elif match == "vermutl. November 1687":
        return build.tei_date(attrib_when="1687-11")
    elif match == "evtl. Anfang/Mitte März 1687":
        return build.tei_date(attrib_not_before="1687-03",
                              attrib_not_after="1687-03")
    # ZEITSPANNE: ANFANG / MITTE / ENDE MONAT
    elif match == "Anfang Oktober 1688":
        return build.tei_date(attrib_not_before="1688-10",
                              attrib_not_after="1688-10")
    elif match == "Anfang Februar 1689":
        return build.tei_date(attrib_not_before="1689-02",
                              attrib_not_after="1689-02")
    elif match == "Anfang Januar 1690":
        return build.tei_date(attrib_not_before="1690-01",
                              attrib_not_after="1690-01")
    elif match == "Anfang Mai 1691":
        return build.tei_date(attrib_not_before="1691-05",
                              attrib_not_after="1691-05")
    elif match == "Ende Dezember 1682":
        return build.tei_date(attrib_not_before="1682-12",
                              attrib_not_after="1682-12")
    elif match == "Ende Juli 1688":
        return build.tei_date(attrib_not_before="1688-07",
                              attrib_not_after="1688-07")
    elif match == "Mitte/Ende September 1690":
        return build.tei_date(attrib_not_before="1690-09",
                              attrib_not_after="1690-09")
    elif match == "Mitte Juni 1691":
        return build.tei_date(attrib_not_before="1691-06",
                              attrib_not_after="1691-06")
    elif match == "Mitte bis Ende April 1691":
        return build.tei_date(attrib_not_before="1691-04",
                              attrib_not_after="1691-04")
    elif match == "Ende August/Anfang September 1683":
        return build.tei_date(attrib_not_before="1683-08",
                              attrib_not_after="1683-09")
    elif match == "Ende Januar/ Februar 1684":
        return build.tei_date(attrib_not_before="1684-01",
                              attrib_not_after="1684-02")
    elif match == "Ende Februar/Anfang März 1691":
        return build.tei_date(attrib_not_before="1691-02",
                              attrib_not_after="1691-03")
    elif match == "Ende April/Anfang Mai 1692":
        return build.tei_date(attrib_not_before="1692-04",
                              attrib_not_after="1692-05")
    elif match == "Juli/Anfang August 1684":
        return build.tei_date(attrib_not_before="1684-07",
                              attrib_not_after="1684-08")
    elif match == "November/Anfang Dezember 1691":
        return build.tei_date(attrib_not_before="1691-11",
                              attrib_not_after="1691-12")
    elif match == "wahrsch. Anfang Juni 1692":
        return build.tei_date(attrib_not_before="1692-06",
                              attrib_not_after="1692-06")
    elif match == "wahrsch. Anfang/Mitte September 1683":
        return build.tei_date(attrib_not_before="1683-09",
                              attrib_not_after="1683-09")
    elif match == "wahrsch. Anfang/Mitte Februar 1690":
        return build.tei_date(attrib_not_before="1690-02",
                              attrib_not_after="1690-02")
    elif match == "wahrsch. Mitte/Ende August 1690":
        return build.tei_date(attrib_not_before="1690-08",
                              attrib_not_after="1690-08")
    elif match == "wahrsch. Mitte August 1683":
        return build.tei_date(attrib_not_before="1683-08",
                              attrib_not_after="1683-08")
    elif match == "wahrsch. August/Anfang September 1690":
        return build.tei_date(attrib_not_before="1690-08",
                              attrib_not_after="1690-09")
    elif match == "vermutl. Anfang April 1690":
        return build.tei_date(attrib_not_before="1690-04",
                              attrib_not_after="1690-04")
    elif match == "vermutl. Mitte März 1689":
        return build.tei_date(attrib_not_before="1689-03",
                              attrib_not_after="1689-03")
    elif match == "vermutl. Mitte Juni 1691":
        return build.tei_date(attrib_not_before="1691-06",
                              attrib_not_after="1691-06")
    elif match == "vermutl. Ende Juni/Anfang Juli 1688":
        return build.tei_date(attrib_not_before="1688-06",
                              attrib_not_after="1688-07")
    elif match == "vermutl. Ende März/Anfang April 1690":
        return build.tei_date(attrib_not_before="1690-03",
                              attrib_not_after="1690-04")
    elif match == "vermutl. Ende Juni 1691":
        return build.tei_date(attrib_not_before="1691-06",
                              attrib_not_after="1691-06")
    # ZEITSPANNE: GENAUES DATUM
    elif match == "17./18. März 1690":
        return build.tei_date(attrib_from="1690-03-17",
                              attrib_to="1690-03-18")
    elif match == "4./14. April 1690":
        return build.tei_date(attrib_from="1690-04-04",
                              attrib_to="1690-04-14")
    elif match == "19. oder 20. August 1689":
        return build.tei_date(attrib_not_before="1689-08-19",
                              attrib_not_after="1689-08-20")
    elif match == "2. oder 3. Januar 1691":
        return build.tei_date(attrib_not_before="1691-01-02",
                              attrib_not_after="1691-01-03")
    elif match == "zwischen 24. und 27. August 1683":
        return build.tei_date(attrib_not_before="1683-08-24",
                              attrib_not_after="1683-08-27")
    elif match == "ca. 29./30. November 1688":
        return build.tei_date(attrib_not_before="1688-11-29",
                              attrib_not_after="1688-11-30")
    # VOR / NACH EINEM DATUM
    elif match == "vor dem 7. Dezember 1686":
        return build.tei_date(attrib_not_after="1686-12-06")
    elif match == "kurz vor dem 21. April 1689":
        return build.tei_date(attrib_not_after="1689-04-20")
    elif match == "kurz vor dem 3. September 1692":
        return build.tei_date(attrib_not_after="1692-09-02")
    elif match == "nach dem 21. März 1690":
        return build.tei_date(attrib_not_before="1690-03-22")
    elif match == "nach dem 7. Januar 1691":
        return build.tei_date(attrib_not_before="1691-01-08")
    elif match == "nach dem 18. April 1691":
        return build.tei_date(attrib_not_before="1691-04-19")
    # GENAUES DATUM: ERMITTELT
    elif match == "ca. Anfang 1687":
        return build.tei_date(attrib_when="1687-01")
    elif match == "ca. 25. Januar 1688":
        return build.tei_date(attrib_when="1688-01-25")
    elif match == "ca. 27. März 1688":
        return build.tei_date(attrib_when="1688-03-27")
    elif match == "ca. 26. November 1688":
        return build.tei_date(attrib_when="1688-11-26")
    elif match == "ca. 28. November 1688":
        return build.tei_date(attrib_when="1688-11-28")
    elif match == "ca. 14. Januar 1689":
        return build.tei_date(attrib_when="1689-01-14")
    elif match == "ca. 29. März 1692":
        return build.tei_date(attrib_when="1692-03-29")
    elif match == "ca. 2. April 1692":
        return build.tei_date(attrib_when="1692-04-02")
    elif match == "wahrsch. 6. Juli 1692":
        return build.tei_date(attrib_when="1692-07-06")
    else:
        # parse given date string
        try:
            # test if day is present by variant default values
            var1 = datetime.strptime('2001-01-01', '%Y-%m-%d')
            var2 = datetime.strptime('2002-02-02', '%Y-%m-%d')
            res1 = parser.parse(match, MonthParser(), default=var1).strftime("%Y-%m-%d")
            res2 = parser.parse(match, MonthParser(), default=var2).strftime("%Y-%m-%d")
            if res1 == res2:    # equal if no default value was used
                # if day is present return YYYY-MM-DD
                return build.tei_date(attrib_when=parser.parse(
                    match, MonthParser()).strftime("%Y-%m-%d"))
            else:
                # if day is not present return YYYY-MM
                return build.tei_date(attrib_when=parser.parse(
                    match, MonthParser()).strftime("%Y-%m"))
        except (TypeError, ValueError):
            print("could not find date in given line:")
            print(line)
            print("")
            return None

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ main routine to create correspondence metadata file ~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


def main():
    """main routine to create correspondence metadata file"""
    # create <TEI>
    root = build.tei_root()
    # create <teiHeader>
    header = build.tei_header()
    # create <titleStmt>
    title = build.tei_title("Christian Thomasius Briefwechsel 1679–1692")
    editor = build.tei_editor("Donatus Herre ")
    email = build.tei_email("donatus.herre@izea.uni-halle.de")
    editor.append(email)
    title_stmt = build.tei_title_stmt([title, editor])
    # create <publicationStmt>
    publisher = build.tei_publisher(
        build.tei_ref("Interdisziplinäres Zentrum für die Erforschung der Europäischen Aufklärung",
                      "https://www.izea.uni-halle.de"))
    url = "https://gitlab.informatik.uni-halle.de/dikon/tbw-cmif/-/raw/master/data/briefverzeichnis.xml"
    idno = build.tei_idno(url)
    pub_date = build.tei_date(attrib_when=datetime.today().strftime("%Y-%m-%d"))
    availability = build.tei_availability(build.tei_license())
    pub_stmt = build.tei_publication_stmt([publisher, idno,
                                           pub_date, availability])
    # create <sourceDesc>
    ref_url = "https://opendata.uni-halle.de//handle/1981185920/33426"
    bibl = build.tei_bibl("Christian Thomasius Briefwechsel. Band 1: 1679–1692. " + \
                          "Hg. von Frank Grunert, Matthias Hambrock und Martin Kühnel " + \
                          "unter Mitarbeit von Andrea Thiele. Berlin; Boston: de Gruyter, 2017. " + \
                          "DOI: ", "hybrid", domain=ref_url)
    ref = build.tei_ref("10.1515/9783110471328", "https://doi.org/10.1515/9783110471328")
    bibl.append(ref)
    source_desc = build.tei_source_desc([bibl])
    # create <fileDesc> with child nodes
    #   <titleStmt>, <publicationStmt> and <sourceDesc>
    file_desc = build.tei_file_desc([title_stmt, pub_stmt, source_desc])
    # add <fileDesc> to <teiHeader>
    header.append(file_desc)
    # create <profileDesc>
    profile_desc = build.tei_profile_desc()
    with open("data/briefverzeichnis.json", 'r', encoding='utf-8') as f:
        metadata = json.load(f)
        for num in metadata:
            # create <correspAction> with @type="sent"
            author = identify(metadata[num]["author"])
            place_date = metadata[num]["place-date"]
            sent_place = place(place_date)
            sent_date = date(place_date)
            if sent_place is not None and sent_date is not None:
                sent = build.tei_corresp_action("sent",
                                                children=author+[sent_place,
                                                                 sent_date])
            elif sent_place is not None and sent_date is None:
                sent = build.tei_corresp_action("sent",
                                                children=author+[sent_place])
            elif sent_place is None and sent_date is not None:
                sent = build.tei_corresp_action("sent", children=author+[sent_date])
            else:
                sent = build.tei_corresp_action("sent", children=author)
            # create <correspAction> with @type="received"
            addressee = identify(metadata[num]["addressee"])
            received = build.tei_corresp_action("received", children=addressee)
            corresp_desc = build.tei_corresp_desc(attrib_key=num,
                                                  attrib_source="#" + bibl.get(build.ns_xml("id")),
                                                  children=[sent, received])
            # add <correspDesc> to <profileDesc>
            profile_desc.append(corresp_desc)
    # add <profileDesc> to <teiHeader>
    header.append(profile_desc)
    # add <teiHeader> to <TEI>
    root.append(header)
    # create <text> element
    text = build.tei_text_empty()
    # add <text> to <TEI>
    root.append(text)
    # save <TEI> as XML
    local.writer(root, file="briefverzeichnis.xml", path=OUT)


if __name__ == '__main__':
    main()
