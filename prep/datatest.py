#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
script to check correspondence
data from json file found at:

  ../data/briefverzeichnis.json

Creator:          D. Herre
GitLab:     dikon/tbw-cmif

Created:        2020-05-03
Last Modified:  2020-06-09
"""

import re
import os
import json

metadata = []
with open("data/briefverzeichnis.json", 'r', encoding='utf-8') as f:
    metadata = json.load(f)

reCorrAdd = re.compile('\\[.*?\\]')
reCorrAdd2 = re.compile('\\[|\\]')


def authors_unique():
    """get list of unique authors"""
    authors = []
    for num in metadata:
        authors.append(metadata[num]["author"])
    authors = list(set(authors))
    authors.sort()
    return authors


def addressees_unique():
    """get list of unique addressees"""
    addressees = []
    for num in metadata:
        addressees.append(metadata[num]["addressee"])
    addressees = list(set(addressees))
    addressees.sort()
    return addressees


def entities_unique():
    """get list of unique entities (authors + addressees)"""
    authors = authors_unique()
    addressees = addressees_unique()
    entities = list(set(authors + addressees))
    entities.sort()
    return entities


def save_entities():
    """save list of unique entities to plain text file"""
    entities = entities_unique()
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/entities.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(entities))


def save_entities_template():
    """save list of unique entities to json template file"""
    entities = entities_unique()
    template = {}
    for e in entities:
        template[e] = ""
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/entities.json", "w+", encoding="utf-8") as f:
        s = json.dumps(template, ensure_ascii=False, indent=2)
        f.write(s)


def places_unique():
    """get list of unique places"""
    places = []
    exclude = ["[Ende 1689/Anfang 1690]",
               "[vermutl. November] 1687",
               "[wahrsch. zweite Jahreshälfte 1690]"]
    for num in metadata:
        place = metadata[num]["place-date"].split(",")[0].strip()
        if place not in exclude:
            places.append(place.strip())
    places = list(set(places))
    places.sort()
    return places


def save_places():
    """save list of unique places to plain text file"""
    places = places_unique()
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/places.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(places))


def save_places_template():
    """save list of unique places to json template file"""
    places = places_unique()
    template = {}
    for p in places:
        template[p] = ""
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/places.json", "w+", encoding="utf-8") as f:
        s = json.dumps(template, ensure_ascii=False, indent=2)
        f.write(s)


def dates_unique():
    """get list of unique dates"""
    dates = []
    include = ["[Ende 1689/Anfang 1690]",
               "[vermutl. November] 1687",
               "[wahrsch. zweite Jahreshälfte 1690]"]
    for num in metadata:
        place_date = metadata[num]["place-date"].split(",")
        if len(place_date) > 1:
            dates.append(place_date[1].strip())
        else:
            if place_date[0] in include:
                dates.append(place_date[0].strip())
            else:
                dates.append("null")
    dates = list(set(dates))
    dates.sort()
    return dates


def save_dates():
    """save list of unique dates to plain text file"""
    dates = dates_unique()
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/dates.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(dates))


def additions():
    """get dict with editorial additions of dates and places"""
    dates = []
    dates_part = []
    entities = []
    entities_part = []
    places = []
    places_part = []
    total = []
    for num in metadata:
        # AUTHOR
        e = metadata[num]["author"]
        res = reCorrAdd.search(e)
        if res is not None:
            start, end = res.span()
            if end - start == len(e):
                entities.append(reCorrAdd2.sub("", e).strip())
            else:
                entities_part.append(reCorrAdd2.sub("", e[start:end]).strip())
        e = metadata[num]["addressee"]
        res = reCorrAdd.search(e)
        # ADDRESSEE
        if res is not None:
            start, end = res.span()
            if end - start == len(e):
                entities.append(reCorrAdd2.sub("", e).strip())
            else:
                entities_part.append(reCorrAdd2.sub("", e[start:end]).strip())
        # PLACE-DATE
        lt = metadata[num]["place-date"]
        res = reCorrAdd.search(lt)
        if res is not None:  # addition is present
            if len(list(reCorrAdd.finditer(lt))) == 1:  # single addition
                found = False
                start, end = res.span()
                if end - start == len(lt):  # place and date added completely
                    found = True
                    if "," in lt:
                        lt_split = lt.split(",")
                        places.append(reCorrAdd2.sub("", lt_split[0]).strip())
                        dates.append(reCorrAdd2.sub("", lt_split[1]).strip())
                    else:   # no place given, only date added!
                        if res.string in ["[Ende 1689/Anfang 1690]",
                                          "[wahrsch. zweite Jahreshälfte 1690]"]:
                            dates.append(reCorrAdd2.sub("", res.string).strip())
                        else:
                            total.append(reCorrAdd2.sub("", res.string).strip())
                else:   # place and/or date added partly
                    lt_sub = lt[start:end]
                    if "," in lt_sub:   # place and date or part of date added
                        found = True
                        lt_split = lt_sub.split(",")
                        if start == 0:  # place added completely
                            places.append(reCorrAdd2.sub("", lt_split[0]).strip())
                        else:   # place added partly
                            places_part.append(reCorrAdd2.sub("", lt_split[0]).strip())
                        if end == len(lt):  # date added completely
                            dates.append(reCorrAdd2.sub("", lt_split[1]).strip())
                        else:   # date added partly
                            dates_part.append(reCorrAdd2.sub("", lt_split[1]).strip())
                    pos = lt.find(",")
                    if end == pos:  # place added
                        found = True
                        if start == 0:  # place added completely
                            places.append(reCorrAdd2.sub("", lt_sub).strip())
                        else:   # place added partly
                            places_part.append(reCorrAdd2.sub("", lt_sub).strip())
                    if start > pos:  # date or part of date added
                        found = True
                        if start == pos+2:
                            dates.append(reCorrAdd2.sub("", lt_sub).strip())
                        else:   # date added partly
                            dates_part.append(reCorrAdd2.sub("", lt_sub).strip())
                if not found:
                    print("addition not found:", lt)
            else:   # mutiple additions
                found = []
                pos = lt.find(",")
                for res in list(reCorrAdd.finditer(lt)):
                    start, end = res.span()
                    if end == pos:  # place added
                        found.append(True)
                        if start == 0:
                            places.append(reCorrAdd2.sub("", lt[start:end]).strip())
                        else:
                            places_part.append(reCorrAdd2.sub("", lt[start:end]).strip())
                    if start > pos:  # date or part of date added
                        found.append(True)
                        if start == pos+2:
                            dates.append(reCorrAdd2.sub("", lt[start:end]).strip())
                        else:
                            dates_part.append(reCorrAdd2.sub("", lt[start:end]).strip())
                if len(found) != len(list(reCorrAdd.finditer(lt))):
                    print("unknown addition:", lt)
        else:
            continue
    dates = list(set(dates))
    dates.sort()
    dates_part = list(set(dates_part))
    dates_part.sort()
    entities = list(set(entities))
    entities.sort()
    entities_part = list(set(entities_part))
    entities_part.sort()
    places = list(set(places))
    places.sort()
    places_part = list(set(places_part))
    places_part.sort()
    total = list(set(total))
    total.sort()
    return {"dates": dates, "dates_part": dates_part,
            "entities": entities, "entities_part": entities_part,
            "places": places, "places_part": places_part,
            "total": total} if total != [] else \
        {"dates": dates, "dates_part": dates_part,
         "entities": entities, "entities_part": entities_part,
         "places": places, "places_part": places_part}


def save_additions():
    """save lists with editorial additions of dates and places"""
    addit = additions()
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/dates_added.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(addit["dates"]))
    with open("prep/datatest/dates_part_added.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(addit["dates_part"]))
    with open("prep/datatest/entities_added.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(addit["entities"]))
    with open("prep/datatest/entities_part_added.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(addit["entities_part"]))
    with open("prep/datatest/places_added.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(addit["places"]))
    with open("prep/datatest/places_part_added.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(addit["places_part"]))
    if "total" in addit:
        with open("prep/datatest/total_added.txt", "w+", encoding="utf-8") as f:
            f.write("\n".join(addit["total"]))


if __name__ == '__main__':
    save_dates()
    save_places()
    save_entities()
    save_additions()
    save_entities_template()
    save_places_template()
