1. April 1688
1. Dezember 1682
1. Dezember 1688
1. Februar 1687
1. Februar 1691
1. Januar 1691
1. November 1690
10. April 1688
10. April 1689
10. August 1689
10. Februar 1689
10. Juni 1689
10. März 1679
10. März 1691
10. November 1686
10. Oktober 1688
11. April 1690
11. April [1685]
11. August 1688
11. Dezember 1690
11. Februar 1688
11. Februar 1691
11. März 1691
12. August 1684
12. Mai 1688
12. September 1688
13. Januar 1689
13. Juli 1689 (I)
13. Juli 1689 (II)
13. November 1690
14. Januar 1689
14. Juni 1689
14. März 1688
14. März 1689
14. März 1691
15. April 1689
15. April 1690
15. Dezember 1688
15. Januar 1689
15. Mai 1688
15. März 1690
15. November 1688
15. September 1683
15. September 1690
15. September 1691
16. Juni 1691
16. Oktober 1688
17. Dezember 1692
17. Juli 1688
17. März 1688
17. März 1691
17. Oktober 1691
17./18. März 1690
18. August 1690
18. Februar 1688
18. Juni 1692
18. Mai 1690
18. September 1688
19. April 1689
19. April 1689]
19. Juni 1688
19. oder 20. August 1689]
2. Dezember [1685]
2. Januar 1683
2. Mai 1691
2. September 1689
2. September 1690
2. oder 3. Januar] 1691
20. August 1688
20. August 1689
20. Dezember 1691
20. Dezember 1692
20. Februar 1683
20. Februar 1690
20. Juli 1689
20. Juni 1692
21. August 1689
21. August 1689]
21. Juli 1683
21. Juni 1689
21. Mai 1692
21. Oktober 1691
22. April 1686
22. August 1688
22. Dezember 1688
22. Juli 1688
22. November 1690
23. April 1689
23. August 1689]
23. Dezember 1688
23. Februar 1692
23. Januar 1688
23. Januar 1689
23. Juni 1688
23. März 1687
23. September 1689
23.] Februar 1692
24. August 1689
24. Januar 1689
24. März 1688
24. März 1691
24. November 1688
24. Oktober 1691
25. April 1691
25. Februar 1688
25. Februar 1689
25. Februar 1689]
25. Juli 1688
25. März 1690
25. November 1688
26. August 1688
26. August 1690
26. August 1690 (I)
26. August 1690 (II)
26. Januar 1690
26. Januar [1684]
26. November [1692]
27. Februar 1692
27. Juni 1689
27. Juni 1689 (I)
27. Juni 1689 (II)
27. Juni 1691
28. April 1691
28. April 1692
28. August 1683
28. August 1688
28. August 1689
28. Dezember 1688
28. März 1689
28. November 1690
28. Oktober 1690
29. April 1690
29. November 1688
3. April 1689
3. Dezember 1688
3. Juli 1689
3. November 1691
3. September 1690
30. April 1691
30. April 1692
30. August 1690
30. Dezember 1688
30. Dezember 1692
30. September 1690
31. August 1687
31. Dezember 1692
31. Mai 1689
31. März 1688
31. Oktober 1691
4. April 1688
4. April 1689
4. Februar 1690
4. Januar 1691
4. Juli 1688
4. Juni 1681
4. März 1688
4. November 1691
4. September 1683
4./14. April 1690
5. April 1692
5. Januar 1691
5. März 1692
5. November 1686]
5. Oktober 1687
6. August 1689
6. Dezember 1692
6. Juni 1691
6. Mai 1692
6. März 1685
6. März 1691
6. Oktober 1690
7. April 1689
7. August 1689
7. August 1690
7. Dezember 1688
7. Februar 1691
7. Januar 1691
7. Juli 1688
7. Mai 1687
7. Oktober 1688
7. September 1679
8. Dezember 1688
8. Juli 1692
8. Juli 1692]
8. Juni 1688
8. Juni 1689
8. November [1692]
8. September 1690
9. April 1687
9. April 1692
9. August 1690
9. Juni 1686
9. März 1692
9. [März] 1692
Anfang 1679]
Anfang Februar 1689]
Anfang Januar 1690
Anfang Oktober] 1688
Ende 1692]
Ende April/Anfang Mai 1692]
Ende August/Anfang September 1683]
Ende Dezember 1682]
Ende Februar/Anfang März] 1691
Ende Januar/ Februar 1684]
Ende Juli] 1688
Februar 1683]
Juli/Anfang August 1684]
Mai 1691
Mitte Juni 1691]
Mitte bis Ende April] 1691
Mitte/Ende September] 1690
November 1685]
November/Anfang Dezember] 1691
Oktober 1692
[Anfang Mai] 1691
[Ende 1689/Anfang 1690]
[kurz vor dem 21. April] 1689
[vermutl. Ende März/Anfang April] 1690
[vermutl. November] 1687
[wahrsch. 6. Juli] 1692
[wahrsch. zweite Jahreshälfte 1690]
ca. 14. Januar 1689]
ca. 2. April 1692]
ca. 25. Januar 1688]
ca. 26. November] 1688
ca. 27. März] 1688
ca. 28. November] 1688
ca. 29. März 1692]
ca. 29./30. November] 1688
ca. Anfang 1687]
evtl. Anfang/Mitte März 1687]
kurz vor dem 3. September 1692]
nach dem 18. April] 1691
nach dem 21. März 1690]
nach dem 7. Januar 1691]
vermutl. Anfang April] 1690
vermutl. Ende Juni 1691]
vermutl. Ende Juni/Anfang Juli 1688]
vermutl. Herbst/Ende] 1691
vermutl. Mai] 1691
vermutl. Mitte Juni 1691]
vermutl. Mitte März 1689]
vor dem 7. Dezember 1686]
wahrsch. Anfang Juni 1692]
wahrsch. Anfang/Mitte Februar] 1690
wahrsch. Anfang/Mitte September 1683]
wahrsch. August/Anfang September 1690]
wahrsch. Mai 1685]
wahrsch. Mitte August 1683]
wahrsch. Mitte/Ende August 1690]
wahrsch. zweite Jahreshälfte 1690]
zwischen 24. und 27. August 1683]