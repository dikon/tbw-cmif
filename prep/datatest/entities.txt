Adam Christoph Jacobi
Anonyme(r) Absender
August Hermann Francke
Benjamin Fritsche
Christian Friedrich Kraut
Christian Thomasius
Christianus Philothomas [Vincent Placcius]
Concilium perpetuum der Universität Leipzig
Daniel Ludolph von Danckelmann
Eberhard Christoph Balthasar von Danckelmann
Ein „auswärtiger Freund“ bzw. „Bekandter“
Ein „guter Freund“ (I)
Ein „vertrauter Freund“
Enoch Zobel
Friedrich Adolph von Haugwitz
Georg Lehmann
Gottfried Thomasius
Gottfried von Jena und Carl Ernst Krause
Hans Ernst von Knoch
Hans Haubold von Einsiedel
Herzog Moritz Wilhelm von Sachsen-Zeitz
Hof des Kurfürsten Friedrich III. von Brandenburg
Jacob Born
Jacques Valentin
Johann Alexander Christ
Johann Burchard May
Johann Christoph Becmann
Johann Georg Graevius
Johann Georg II., Fürst von Anhalt-Dessau
Johann Georg Kulpis
Johann Jacob Stübel
Johann Joachim Rothe
Johann Joachim Wolf
Kurfürst Friedrich III. von Brandenburg
Kurfürst Friedrich III. von Brandenburg/Hof des Kurfürsten
Kurfürst Johann Georg III. von Sachsen
Kurfürst Johann Georg III. von Sachsen bzw. das Geheime Ratskollegium
Magdeburgische Landstände
Magdeburgische Regierung zu Halle
Moritz Georg Weidmann
Paul von Fuchs
Philipp Jacob Spener
Rat der Stadt Halle
Rat der Stadt Leipzig
Samuel Pufendorf
Samuel von Pufendorf
Thomasius
Thomasius, August Hermann Francke und Joachim Justus Breithaupt
Thomasius, Joachim Justus Breithaupt und August Hermann Francke
Tobias Pfanner
Unbekannter Absender
Ungenannter Bedienter des Oberhofmarschalls von Haugwitz
Ungenannter Freund
[Christian Friedrich Kraut]
[Christian Thomasius, Joachim Justus Breithaupt und August Hermann Francke]
[Johann Baptist Crophius]
[Johann Christian] Lange
[Johannes] Müller
[Kurfürst Friedrich III. von Brandenburg]
[Kursächsisches Geheimes Ratskollegium]
[Thomasius, Joachim Justus Breithaupt, August Hermann Francke]
[Vincent Placcius]
das Concilium perpetuum der Universität Leipzig
das Geistliche Ministerium zu Leipzig
das Oberkonsistorium Dresden
das kursächsische Geheime Ratskollegium
den Hof des Kurfürsten Friedrich III. von Brandenburg
den Rat der Stadt Halle
den Rat der Stadt Leipzig
den Schöppenstuhl zu Leipzig
die Magdeburgische Regierung zu Halle
die Magdeburgischen Landstände
einen unbekannten Empfänger [Johann Christoph Becmann?]
„Herrn W.“
„M. A.“